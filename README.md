Wiki_Golf
=========

An implementation of Wikipedia Golf using the Flask web micro-framework.

This project is released under the MIT license. The license can be found in license.txt at the root
of the project directory.
