"""Wiki_Golf is made available under the MIT license.  For more details, please
see the file 'license.txt' in the root of the project.
"""
from collections import deque
from py2neo import neo4j
import re
import requests


def collect_data():
	"""Begins the data collection process of making api calls and storing the data in the database.

	The process could start at any page if we are working under the assumption that the page graph
	is a single component.  We arbitrarily choose the page 'Computing' for the starting point of the
	search and go from there."""
