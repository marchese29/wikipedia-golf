"""Script used to ingest the contents of wikipedia into the neo4j database. This
script is designed to run separate from the main app either as a cron-job, or a
one-off task to be executed manually.
"""
from collections import deque
import re

from neo4jrestclient.client import GraphDatabase
import requests

# Establish a connection to the graph database, create the label to place the
# nodes under.
graph_db = GraphDatabase("http://localhost:7474/db/data/")
gdb_nodes = graph_db.labels.create('node')

BASE_URL = 
PATTERN = r'\[\[(?!Image:)(?!Category:)(?!File:).*\]\]'

class Node(object):
    def __init__(self, name):
        """Constructs a node from the provided name.
        CAUTION: The name of the node resolves to where the name redirects.
        """


def place_in_db(node):
    """Places the provided node in the databse."""

# We perform a BFS here to collect the data.  In the interest of preserving
# memory, we use the database to hold the closed set as opposed to an explicit
# list.
openset = deque([1])

while len(openset) > 0:
    state = openset.popleft()
    # TODO: Place the link in the database

    links = []  # TODO: Retrieve the list of links
    if ( "Link is in database" ):
        # TODO: 