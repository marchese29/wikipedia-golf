def convolve(inp, kernel): 
    """Takes the given input and kernel, convolves with them, then returns the
    result.
    """
    out = [[0 for j in inp[0]] for row in inp]
    CENTER = len(kernel) / 2
    for i in range(CENTER, len(inp) - CENTER):
        for j in range(CENTER, len(inp[0]) - CENTER):
            # out[i][j] += (inp[i][j] * kernel[CENTER][CENTER])
            for k in range(-CENTER, CENTER):
                for l in range(-CENTER, CENTER):
                    out[i + k][j + l] += (inp[i + k][j + l] * kernel[k][l])


a = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]
b = [[9, 3, 0], [2, 8, 5], [7, 2, 3]]
print convolve(a, b)